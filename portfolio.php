    <!doctype html>
    <?php
    require 'db_dev.php';
    
    /* Displays user information and some useful messages */
    session_start();
    require 'langs.php';
    // Check if user is logged in using the session variable
    $private = false;
    if($private){
        if ( $_SESSION['logged_in'] != 1 ) {
        $_SESSION['message'] = "You must log in before viewing your profile page!";
        header("location: error.php");    
        }
    
    }
    ?>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1,shrink-to-fit=no">
            <link rel="shortcut icon" href="imgs/Logos/icon.ico" type="image/x-icon">
            <link rel="icon" href="imgs/Logos/icon.ico" type="image/x-icon">
            
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            
            <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
            <script src="js/jquery.backstretch.min.js"></script>
            <script src="js/magnific-popup/jquery.magnific-popup.js"></script>
                    
            <link href="css/portfolio.css" rel="stylesheet">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
            <link href="https://fonts.googleapis.com/css?family=Lato:400,700|Montserrat:400,7S00" rel="stylesheet">
            <link rel="preconnect" href="https://fonts.googleapis.com">
            <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
            <link href="https://fonts.googleapis.com/css2?family=Righteous&display=swap" rel="stylesheet">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
            <link href="js/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">
            <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css" rel="stylesheet" />
            <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.theme.default.min.css" rel="stylesheet" />
            <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>
            <script src="js/scroll.js"></script>
            <script src="js/contact.js"></script>

            <title>Alejandro Canela Portfolio - Game Programmer</title>
        </head>
    
        <body>
            <nav class = "navbar navbar-expand-lg navbar-dark fixed-top nav-bg navbar-no-bg " id="navi">
            <div class="container">
                <img class="mx-auto" src="imgs/Logos/main128.png" alt="">
                <a class="navbar-brand" href="#">ALEJANDRO CANELA MENDEZ</a>
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navmenu" aria-controls="navmenu" aria-expanded="false" aria-label="Toggle Navigation">
                        <span class = "navbar-toggler-icon"></span>
                    </button>
                    <div class = "collapse navbar-collapse" id = "navmenu">
                        <ul class="navbar-nav ml-auto">
                            <li class= "nav-item">
                                <a class="nav-link scroll-link" href="#about" >About</a>
                            </li>
                            <li class= "nav-item">
                                <a class="nav-link scroll-link" href="#portfolio">Portfolio</a>
                            </li>
                            <li class="nav-item" >
                                <a class= "nav-link scroll-link" href="#contact" >Contact</a>
                            </li>
                            <!--<?php
    
                            if($_SESSION["logged_in"] == 1) { 
                            ?>
                            <li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Admin Panel</a>
                              <div class="dropdown-menu">
                                <a class="dropdown-item" href="add_project.php">Add Project</a>
                                <a class="dropdown-item" href="logout.php">Logout</a>
                              </div>
                            </li>
                            <?php } else { ?>
                                <li class="nav-item" >
                                <a class= "nav-link" href="admin.php" >Admin</a>
                            </li>
                            <?php
                            }
                        ?>-->
                        </ul>
                    </div>
            </div>
            </nav>
    
            <div class="intro">
                        <div class = "inner-div">
                            <div class="container ">
                                <img class="img-fluid rounded-circle mb-5 d-block mx-auto" src="imgs/profile.jpg" alt="">
                                <div class="row text-center">
                                    <div class="col-md-8 offset-md-2 text">
                                        <h1>Another Game Programmer Portfolio</h1>
                                        <div>
                                            <h2>VR - UE4 - GAME DEVELOPER</h2>
                                        </div>
                                        
                                        <div class="text-center mt-4">
                                            <form method="get" action="data/CV.pdf">
                                            <button class="btn btn-xl btn-outline-light" type="submit"> <i class="fa fa-download mr-2"></i>Download my CV now!</button>
                                            </form>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>   
            </div> <!--End intro section-->
           
             <!--Start about me section-->
            <div class="about first-bg">
                <div class="container">
                
                    <h1 class ="text-center text-uppercase about-header-text">About Me</h1>
                    
                    <div class="row text-center">
                            <p class = "aboutme text">
                            Hi all! I am Alejandro Canela, a C++ developer and game programmer who was born in Valencia, Spain. <br><br>
                            I like to focus on creating nice, reliable and readable code making use of programming patterns and rules of
                            thumb always I can. My main interests in video games programming are focused on the core engine
                            principally, however, I feel comfortable in gameplay programming and creating new mechanics. <br><br>
                            My strong skills are programming in C++. Referring to graphics API's, I have been working with OpenGL and
                            GLSL for more than two years. <br><br>
                            I am fluent in English ( written and spoken) and I have a basic level of German. <br><br>
                        </p>
                     </div>
                     
                              <!--Start top skills section-->
                              <div class="top-skills">
                                 <div class="container">
                                   <!--  <h1 class ="text-center text-uppercase education-header-text">Top Skills</h1>-->
                                     <div class="left_50">
                                         <div id="owl-example" class="owl-carousel">
                                         
                                             <div class="owl_text text-uppercase">
                                                 <p class="mt-20">Gameplay Development</p>
                                             </div>
                                             
                                             <div class="owl_text text-uppercase">
                                                 <p class="mt-20"> 2+ Virtual Reality Development</p>
                                             </div>
                                                                                         
                                             <div class="owl_text text-uppercase">
                                                 <p class="mt-20">3+ years of Unreal Engine 4 professional experience</p>
                                             </div>
                                             
                                             <div class="owl_text text-uppercase">
                                                 <p class="mt-20">3+ years of C++ professional experience</p>
                                             </div>
                                             
                     
                                                                   
                                         </div> 
                                      </div>   
                                 </div>
                             </div> <!--End top skills section-->
                     
                </div>
            </div><!--End about me section-->
             
             <!--Start education section-->
             <div class="education second-bg">
                <div class="container">
                
                    <h1 class ="text-center text-uppercase education-header-text">Education</h1>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class= "col-md-6 col-lg-4">
                                <div class = "card-box">
                                    <div class="card-thumbnail">
                                        <img src="imgs/teesside_logo.png" class="img-fluid" alt="">
                                    </div>
                                    <h3>Teesside University</h3>
                                    <p class="text-secondary">Bachelor with Honours in Computer Games Programming</p>
                                     <p class="education-period">2017 - 2018</p>
                                </div>
                            </div>
                  
                    
                            <div class= "col-md-6 col-lg-4">
                                <div class = "card-box">
                                    <div class="card-thumbnail">
                                        <img src="imgs/esat.png" class="img-fluid" alt="">
                                    </div>
                                    <h3>ESAT University</h3>
                                    <p class="text-secondary">High National Diploma in Videogames Programming by Edexcel</p>
                                    <p class="education-period">2013 - 2016</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!--End education section-->
            
    

           <div class="timeline first-bg">
             <div class="container experience">
                 <h1 class ="text-center text-uppercase experience-header-text">Professional Experience</h1>
                 <ul>
                   <!--Degestec-->
                   <li>
                     <div class = "experience-outer">
                         <div class = "experience-inner">
                           <time>Nov 2018 - Dic 2019</time>
                           <h3 class ="text-center text-uppercase company-text"> Degestec </h3>
                           <h5 class ="text-center job-title-text"> Junior C++ Programmer </h5>
                           <div class = "company-description">
                               During my time at Degestec I develop an online Bingo game from scratch using C++ and OpenDL
                               I also implemented a custom network API.
                           </div>
                         </div>
                     </div>
                   </li>
                   <!--End Degestec-->
                   <!--RW1-->
                   <li>
                     <div class = "experience-outer">
                         <div class = "experience-inner">
                           <time>Nov 2019 - Apr 2021</time>
                           <h3 class ="text-center text-uppercase company-text"> Real World One </h3>
                            <h5 class ="text-center job-title-text"> VR Developer </h5>
                           <div class = "company-description">
                               During my time at Degestec I develop an online Bingo game from scratch using C++ and OpenDL
                               I also implemented a custom network API.
                           </div>
                         </div>
                     </div>
                   </li>
                   <!--End RW1-->
                   <!--Hologate-->
                   <li>
                     <div class = "experience-outer">
                         <div class = "experience-inner">
                           <time>Apr 2021 - present</time>
                           <h3 class ="text-center text-uppercase company-text"> Hologate </h3>
                           <h5 class ="text-center job-title-text"> VR - UE4 Game Developer </h5>
                           <div class = "company-description">
                               During my time at Degestec I develop an online Bingo game from scratch using C++ and OpenDL
                               I also implemented a custom network API.
                           </div>
                         </div>
                     </div>
                   </li>
                   <!--End Hologate-->
                 </ul>
             </div>
           </div>    
        
            <div class="portfolio second-bg">
                <div class="container">
                    <h1 class ="text-center text-uppercase experience-header-text">Portfolio</h1>
                    <hr class="star-dark mb-5">
                    <div class="row">
                        <?php
                            $sql = "SELECT * FROM projects";
                            $result = $mysqli->query($sql);
        
                            if (! $result){
                                throw new My_Db_Exception('Database error: ' . mysql_error());
                            }
                             $counter = 0;
                             $max_columns = 3;                   
        
                            if( $result->num_rows != 0) {
                                while($row = $result->fetch_assoc()){
                                    $counter = $counter + 1;
                                    if ($counter > $max_columns) {
                                        echo '<div class="row">';
                                        $counter = 0;
                                    }
                                    else
                                    {
                                        $final_name = str_replace(" ","_",$row["name"]);// $row["name"];
                                         echo '<div class= "col-md-6 portfolio-box">
                                            <div class ="portfolio-box-image d-block ">
                                            <a class = "portfolio-link" href="#'.$final_name.'"><img class="img-fluid" src = "imgs/projects/'.$row["image"] .'" alt=""></a>
                                            </div>
                                            <h3> <a class ="portfolio-link text-uppercase" href="#'.$final_name.'">'.$row["name"].'<a/><i class="fas fa-angle-right"></i></h3>
                                            <p class="wrap_desc">' . $row["description"] . '</p>
                                                </div>
                    
                                                ';
                                    }       
                                }
                            }
                        ?>  
                    </div>
                </div>
            </div>
            
    <!--Contact-->
    
            <div class="contact first-bg">
            <div class="container">
                <h1 class ="text-center light-text text-uppercase">Contact Me</h1>
                <hr class="star-light mb-5">
                <div class="row">
                    <div class="col-lg-8 mx-auto">
    
                        <form name="sentMessage" id="contactForm" novalidate="novalidate">
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Name</label>
                                    <input class="form-control" id="name" type="text" placeholder="Name" required="required" data-validation-required-message="Please enter your name.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
    
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Email Address</label>
                                    <input class="form-control" id="email" type="email" placeholder="Email Address" required="required" data-validation-required-message="Please enter your email address.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
    
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Phone Number</label>
                                    <input class="form-control" id="phone" type="tel" placeholder="Phone Number" required="required" data-validation-required-message="Please enter your phone number.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
    
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Message</label>
                                    <textarea class="form-control" id="message" rows="5" placeholder="Message" required="required" data-validation-required-message="Please enter a message."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
    
                            <!--<div class="g-recaptcha" data-sitekey="6LfGb2gUAAAAAEX7u7KbKLiBgy1bBPfDXSIA5tZd" name ="capcha" id="capcha">
                          
                            </div>  -->
                            <br>
                            <div id="success"></div>
                            <div class="form-group">
                                <button type="submit" id="submit" class="btn btn-primary btn-xl">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            </div>
    
        <footer class="footer text-center">
          <div class="container">
            <div class="row">
    
    
              <div class="col-md-4 mb-5 mb-lg-0">
                <h4 class="text-uppercase mb-4">Around the Web</h4>
                <ul class="list-inline mb-0">
    
                  <li class="list-inline-item">
                    <a class="btn btn-outline-light btn-social text-center rounded-circle" href="https://plus.google.com/u/2/117853994455264008035" target="_blank">
                    <i class="fab fa-google-plus"></i>
                    </a>
                  </li>
    
                  <li class="list-inline-item">
                    <a class="btn btn-outline-light btn-social text-center rounded-circle" href="https://www.linkedin.com/in/alejandro-canela-125852158/" target="_blank">
                    <i class="fab fa-linkedin"></i>
                    </a>
                  </li>
    
                  <li class="list-inline-item">
                    <a class="btn btn-outline-light btn-social text-center rounded-circle" href="https://bitbucket.org/alcame9/" target="_blank">
                    <i class="fab fa-bitbucket"></i>
                    </a>
                  </li>
    
                </ul>
              </div>
              <div class="col-md-4 mb-5 mb-lg-0"></div>
              <div class="col-md-4 mb-5 mb-lg-0">
                <h4 class="text-uppercase mb-4">Contact</h4>
                <p class="lead mb-0">Alejandro Canela Mendez <br>
                  +34 635 58 25 59 - a.canelame@gmail.com</p>
              </div>
            </div>
          </div>
        </footer>
    
        <div class="copyright py-4 text-center text-white">
          <div class="container">
            <small>Copyright &copy; Alejandro Canela Mendez. 2018</small>
          </div>
        </div>
    
        <div class="d-lg-none position-fixed ">
          <a class="d-block text-center text-white rounded" href="#intro">
            <i class="fa fa-chevron-up"></i>
          </a>
        </div>
    <?php
                        $sql = "SELECT * FROM projects";
                        $result = $mysqli->query($sql);
                        if (! $result){
                            throw new My_Db_Exception('Database error: ' . mysql_error());
                         }
                        if( $result->num_rows != 0) {
                             while($row = $result->fetch_assoc()){
                             $final_name = str_replace(" ","_",$row["name"]);// $row["name"];
                             $url_yt = $row["youtube_url"];
                             $url = str_replace("watch?v=", "embed/",$url_yt);
                            
                             echo '
                            <div class="portfolio-modal mfp-hide" id="'. $final_name.'">
                                <div class="portfolio-modal-dialog bg-white">
                                    <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#"> <i class="fa fa-3x fa-times"></i></a>
                                    <div class="container text-center">
                                        <div class="row">
                                            <div class=" mb-0 mx-auto">
                                                <h2 class="text-secondary text-uppercase">'.$row["name"] . '</h2>
                                                 <hr class="star-dark mb-5">
                                                <img class="img-fluid mb-5" src="imgs/projects/'.$row["image"].'" alt="" height="652" width="1200">
                                                <p class="mb-5 text-justify"> '. $row["description"] .'</p>';
    
                                                if($row["features"]!=""){
                                                    echo '
                                                    <h2 class="text-secondary text-uppercase mb-5">Features</h2>
                                                    <div class ="container mx-auto">
                                                    <ul class ="list-unstyled">F
                                                    ';
                                                    $feature = explode("\n",$row["features"]);
                                                    foreach ($feature as $value) {
                                                        echo '<li>'.$value.'</li>';
                                                    }
                                                        echo '
                                                    </ul>
                                                    </div>';
                                                }else if($row["tasks"]!= ""){
                                                    echo '
                                                    <h2 class="text-secondary text-uppercase mb-5">My tasks</h2>
                                                    <div class ="container mx-auto ">
                                                    <ul class ="list-unstyled">
                                                    ';
                                                    $tasks = explode("\n",$row["tasks"]);
                                                    foreach ($tasks as $value) {
                                                        echo '<li>'.$value.'</li>';
                                                    }
                                                        echo '
                                                    </ul>
                                                    </div>';
                                                }
    
                                                if($row["youtube_url"] != "" ){
                                                
                                                    echo'
                                                    <h2 class="text-secondary text-uppercase mb-5">Check the demo</h2>
                                                    <div class="embed-responsive embed-responsive-16by9">
                                                    <iframe class="embed-responsive-item" src="'.$url.'" allowfullscreen></iframe>
                                                    </div>
                                                    ';
                                                }else if($row["video_src"] != "" ){
                                                    echo '
                                                    <h2 class="text-secondary text-uppercase mb-5">Check the demo</h2>
                                                    <div class="embed-responsive embed-responsive-16by9">
                                                    <video class="embed-responsive-item" width="400" controls>
                                                            <source src="videos/'. $row["video_src"] .'" type="video/mp4">
                                                        </video>
                                                    </div>
                                                    ';              
                                                }
    
                                                if($row["bitbucket_link"] != ""){
                                                    echo '
                                                    <h2 class="text-secondary text-uppercase mb-5">Check the source</h2>
                                                    <div class="container">
                                                        <a class="btn text-center rounded-circle" href="'.$row[bitbucket_link].'" target="_blank">
                                                        <i class="fab fa-bitbucket"></i> </a>
                                                    </div>
                                                    ';       
                                                }
                                    
                                                echo'
                                                    <div class ="col-lg-12 text-center">
                                                        <div class ="row">
                                                            <div class=" mb-5 mx-auto">
                                                                <a class=" mt-5 btn btn-primary btn-xl rounded-pill portfolio-modal-dismiss" href="#">
                                                                    <i class="fa fa-close"></i>
                                                                    Close Project</a>
                                                            </div>
                                                        </div>
                                                    </div>
    
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                    ';
                             }
                        }
    
    
                    ?>  
    
    
        </body>
    
    </html>
    
    
    
    
    
    
