
<!doctype html>

<html>
    <head>
    <title>Site Maintenance</title>
    <style> 
    /*html {
        background: url("imgs/bg.png") no-repeat center center fixed; 
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }*/

    body { text-align: center; padding: 150px;
        background: url("/imgs/code_bg.png") no-repeat center center fixed; 
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        height:100%;
    }
    h1 { color: #fff;font-size: 50px; }
    p { color: #fff;font-size: 20px; }
    body { font: 20px Helvetica, sans-serif; color: #333; }
    article { display: block; text-align: left; width: 650px; margin: 0 auto; }
    a { color: #1ab188; text-decoration: none; }
    a:hover { color: #a0b3b0; text-decoration: none; }
    </style>

</head>
<body class = "body">
<div class = "container">
<article>
    <h1>I&rsquo;ll be back soon!</h1>
    <div>
        <p>Sorry for the inconvenience but I&rsquo;m performing some maintenance at the moment. If you need to contact you can always send me an <a href="mailto:a.canelame@gmail.com">email</a>, otherwise i&rsquo;ll be back online shortly!</p>
        <p>&mdash; Alejandro Canela</p>
    </div>
  <a href="">Admin</a>
</article>
</div>




</body>
</html>