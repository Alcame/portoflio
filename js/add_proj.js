jQuery(document).ready(function(){


     $("#youtube_video").change(function(){
        if($(this).val() !== ''){
            document.getElementById("src_video").disabled = true;
        }else{
            document.getElementById("src_video").disabled = false;
        }
     });

     $("#src_video").change(function(){
        if($(this).val() !== ''){
            document.getElementById("youtube_video").disabled = true;
        }else{
            document.getElementById("youtube_video").disabled = false;
        }
     });

     $("#tasks").change(function(){
        if($(this).val() !== ''){
            document.getElementById("features").disabled = true;
        }else{
            document.getElementById("features").disabled = false;
        }
     });

     $("#features").change(function(){
        if($(this).val() !== ''){
            document.getElementById("tasks").disabled = true;
        }else{
            document.getElementById("tasks").disabled = false;
        }
     });

});
