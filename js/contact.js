function _(id){return document.getElementById(id);}
function submitForm(){
    var name = $("#name").val();
    var email = $("#email").val();
    var phone = $("#phone").val();
    var messag = $("#message").val();
    var capcha = $("#capcha").val();
     $("#success").html('<div class="alert alert-warning">'+capcha+'.</div>');
    if(name =='' || email =='' || phone == '' || messag == ''){
        $("#success").html('<div class="alert alert-warning">Fill all fields.</div>');
    }else{
        _("submit").disabled = true;
        $("#success").html('<div class="alert alert-warning">Please Wait.</div>');

        var formData = new FormData();
        formData.append("name",name);
        formData.append("email",email);
        formData.append("phone",phone);
        formData.append("message",messag);
        var ajax = new XMLHttpRequest();
        ajax.open("POST", "mail/mail.php");
        ajax.onreadystatechange = function(){
            if(ajax.readyState == 4 && ajax.status == 200){
                if(ajax.responseText == "success"){
                    $("#success").html('<div class="alert alert-warning">Thanks to submit it! .</div>');
                    _("submit").disable = false;
                    _("contactForm").reset();
                }else if (ajax.responseText == "fail"){
                    $("#success").html('<div class="alert alert-warning"> Error. The server can not send the email. </div>');
                    _("submit").disabled = false;

                }else if(ajax.responseText == "captcha"){
                    $("#success").html('<div class="alert alert-warning">Captcha not validated! .</div>');
                    _("submit").disable = false;

                }
            }
        }
        ajax.send(formData);
    }
}

$(document).ready(function(){
    $("#submit").click(function(){
        
        submitForm();
        return false;
    });
});