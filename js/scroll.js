function scroll_to(link,nav_height){
    var element_class = link.attr('href').replace('#','.');
    var scroll_to = 0;
    if(element_class != '.top'){
        scroll_to = $(element_class).offset().top - nav_height;
    }

    if($(window).scrollTop() != scroll_to){
        $('html,body').stop().animate({scrollTop: scroll_to},100);
    }
 
}

jQuery(document).ready(function(){
    var navCollapse = function(){
        if( $("#navi").offset().top > 100 ){
            $("#navi").addClass("navbar-shrink");
        }else{
            $("#navi").removeClass("navbar-shrink");            
        }
    };

    $('a.scroll-link').on('click',function(e){
        e.preventDefault();
        $('.navbar-collapse').collapse('hide');
        scroll_to($(this),$('nav').outerHeight());
    });

    $(window).scroll(navCollapse);
    
    $('.intro').backstretch("imgs/bg_code.png");

    $('.portfolio-link').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#username',
        modal: true
      });

    $(document).on('click', '.portfolio-modal-dismiss', function(e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    $("#owl-example").owlCarousel({
        slideSpeed: 300,
        margin: 0,
        paginationSpeed: 400,
        autoplay: false,
        items: 1,
        itemsDesktop: false,
        itemsDesktopSmall: false,
        itemsTablet: false,
        itemsMobile: false,
        loop: true,
        nav: true,
        dots: true,
        navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"]
    });
   navCollapse();

    // define variables
    var items = document.querySelectorAll(".timeline li");

    // check if an element is in viewport
    // http://stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport
    function isElementInViewport(el) {
        var rect = el.getBoundingClientRect();
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <=
            (window.innerHeight || document.documentElement.clientHeight) &&
            rect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    }

    function callbackFunc() {
        for (var i = 0; i < items.length; i++) {
            if (isElementInViewport(items[i])) {
                items[i].classList.add("in-view");
            }
        }
    }

    // listen for events
    window.addEventListener("load", callbackFunc);
    window.addEventListener("resize", callbackFunc);
    window.addEventListener("scroll", callbackFunc);

});


(function () {
    "use strict";


})();
