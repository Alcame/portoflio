<?php
require 'db.php';
session_start();
?>
<!DOCTYPE html>

<html>
    <head>
    <title>Sign-Up/Login Form</title>
    <?php include 'css/css.html';
     ?>
    </head>

    <?php
    if($_SESSION['logged_in'] != null){   
      if($_SESSION['logged_in']==true){
        header("location: portfolio.php");
      }
    }

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
      require 'login.php';
    }
    ?>

    <body>
    <div class="form">
         <div id="login">   
          <h1>Alejandro Canela Portfolio</h1>
          
          <form action="admin.php" method="post" autocomplete="off">
          
            <div class="field-wrap">
            <label>
              Email Address<span class="req">*</span>
            </label>
            <input type="email" required autocomplete="off" name="email"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Password<span class="req">*</span>
            </label>
            <input type="password" required autocomplete="off" name="password"/>
          </div>
                   
          <button type="submit" class="button button-block" name="login">Log In</button>
          
          </form>

 
        </div>
            

      
</div> <!-- /form -->

        <script type="text/javascript" src="js/login.js"></script>
    </body>
</html>