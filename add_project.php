

<?php
/* Displays user information and some useful messages */
session_start();
require 'db.php';
// Check if user is logged in using the session variable
if ( $_SESSION['logged_in'] != 1 ) {
  $_SESSION['message'] = "You must log in before viewing your profile page!";
  header("location: error.php");    
}
else {

}
?>

<!DOCTYPE html>

<html>
    <head>
    <title>New project</title>
    <?php include 'css/css.html';
     ?>

    </head>

 
    ?>

    <body>
    <div class="form" >
         <div id="login">   
          <h1>Alejandro Canela Portfolio</h1>
          
          <form action="upload.php" method="post" autocomplete="off"  enctype="multipart/form-data">
          
          <div class="field-wrap">
            <label>
              Project name<span class="req">*</span>
            </label>
            <input type="text" required autocomplete="on" name="name" id="name"/>
          </div>

         <div class="field-wrap">
          <label>
                Description<span class="req">*</span>
            </label>
              <textarea required autocomplete="off" name="description" id="description"/></textarea>
          </div>

          <div class="field-wrap">
          <label>
              Features<span class="req">*</span>
          </label>
            <textarea  autocomplete="off" name="features" id="features"/></textarea>
          </div>

          <div class="field-wrap">
          <label>
              Tasks<span class="req">*</span>
          </label>
            <textarea  autocomplete="off" name="tasks" id="tasks"/></textarea>
          </div>
        
          <div class="field-wrap">
            <label>
              Youtube Video URL<span class="req">*</span>
            </label>
            <input type="text"  name="youtube_video" id="youtube_video">
          </div>

        <div class="field-wrap lbl">
             Internal Video Source<span class="req">*</span>
            <input type="file" name="src_video" id="src_video"/>
        </div>

         <div class="field-wrap lbl">Picture
            <input required type="file" name="file" id="file">
        </div>

          <div class="field-wrap">
            <label>
              Repository link<span class="req">*</span>
            </label>
            <input type="text" name="bitbucket_link" id="bitbucket_link"/>
          </div>         
          
        <button type ="submit" class="button button-block" name="submit"/>Submit</button>
          
          </form>

 
        </div>
            

      
</div> <!-- /form -->
         <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/jquery-migrate-3.0.0.min.js"></script>
        <script type="text/javascript" src="js/login.js"></script>
        <script type="text/javascript" src="js/add_proj.js"></script>
    </body>
</html>