<?php
require 'db.php';
$dest = "imgs/projects/";
$video_dest = "videos/";
$image_base_name = basename($_FILES["src_video"]["name"]);
$image_target_file = $dest . $image_base_name;
$uploaded = 1;
$image_already_exits=1;
$image_type = strtolower(pathinfo($image_target_file,PATHINFO_EXTENSION));


// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["file"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploaded = 1;
    } else {
        echo "File is not an image.";
        $uploaded = 0;
    }

    if(file_exists($image_target_file)){
        $already_exits = 0;
    }

    if($_FILES["file"]["size"] > 5000000){
        echo "Sorry, the image is too big";
        $uploaded = 0;
    }

    if($image_type != "jpg" && $image_type != "png" && $image_type != "jpeg"
        && $image_type != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploaded = 0;
    }


    //Load the project
    $video_exists = 0;
    $video_base_name = basename($_FILES["src_video"]["name"]);
    $video_target_file = $videos_dest . $video_base_name;

    if($uploaded == 0){
        echo 'Error loading the image';
        $_SESSION['message'] =  "Error loading the image";
        header("location: error.php");

    }else{

        $na = $_POST["name"];
        $des = $_POST["description"];
        $features = $_POST["features"];
        $tasks = $_POST["tasks"];
        $yt_vid = $_POST["youtube_video"];
        $vid_src = $_POST["src_video"];
        $repo = $_POST["bitbucket_link"];
        if(!isset($_POST["youtube_video"])){

            if(file_exists($video_target_file)){
                $video_exists = 0;
            }else{
                $video_exists = 1;
            }

        }
    
        $sql_get_name = "SELECT * FROM projects WHERE name='$na'";
        $result_get_name = $mysqli->query($sql_get_name);

        if($result_get_name->num_rows === 0){

            $sql = 'INSERT INTO projects (id,name,description,image,features,tasks,youtube_url,video_src,bitbucket_link)
            VALUES ("","'. $na .'", "'. $des .'","' . $base_name .'","' . $features .'","'.$tasks. '","' . $yt_vid .'","' . $vid_src .'","'.$repo. ' )';
            $result = $mysqli->query($sql);

            if($result === false){
                $_SESSION['message'] =  "Error: " . $sql . "<br>" . $mysqli->error;
                header("location: error.php");
            }else{
                if ( $already_exits == 0 ){ 
                    $_SESSION['message'] =  'A picture with that name already exits on the server';
                    header("location: error.php");
                }else{
                    if(move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
                        if($video_exists == 1){
                            log("Video exits");
                            if(move_uploaded_file($_FILES["src_video"]["tmp_name"], $video_target_file)) {
                                $_SESSION['message'] =  "The project has been uploaded correctly";
                                header("location: success.php");
                            }else {
                                $_SESSION['message'] = "Sorry, there was an error uploading your video.";
                                header("location: error.php");
                            }   
                        }else{
                            $_SESSION['message'] =  "The project has been uploaded correctly";
                            header("location: success.php");
                        }
                    }else {
                        $_SESSION['message'] = "Sorry, there was an error uploading your picture.";
                        header("location: error.php");
                    }

                }
                

            }
        }else{
            $_SESSION['message'] =  'A project with that name already exits';
            header("location: error.php");

        }
    }

    //$skill_list = explode("\n",$_POST["skills"]);





}else{
    $_SESSION['message'] = "Error submit";
    //header("location: error.php");
}
?>